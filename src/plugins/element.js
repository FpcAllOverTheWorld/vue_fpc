import Vue from 'vue'

//全部导入组件
import ElementUi from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(ElementUi)

//一个个导入
// import { Button } from 'element-ui'
// import { Form , FormItem , Input ,Message} from 'element-ui'
// Vue.use(Button)
// Vue.use(Form)
// Vue.use(FormItem)
// Vue.use(Input)
// Vue.prototype.$message=Message


