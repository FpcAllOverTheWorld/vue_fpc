import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/login.vue'
import Home from '../views/Home.vue'
// import Fpc1 from '../views/fpc1.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/' , redirect: '/login'},
  { path: '/login', component: Login },
  { path: '/home', component: Home }
  // { path: '/fpc1', component: Fpc1 }
]

const router = new VueRouter({
  routes
})

// 挂载路由导航守卫
router.beforeEach((to,from,next)=>{
  // to 将要去哪个路径
  // from 代表从那个路径来的
  // next 一个函数表示放行 next('/login') 强制跳转

  if(to.path=='/login') return next()
  // 获取token
  const Auth_Token= window.sessionStorage.getItem('Auth_Token')
    if(!Auth_Token) return next('/login')
    next()

  })


export default router
