import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
//导入全局样式css
import './assets/css/globo.css'
//导入字体图标
import './assets/fonts/iconfont.css'
//导入axios
import axios from 'axios'


//配置请求路径
axios.defaults.baseURL='http://localhost:8080/'
//挂载axios 全局都可以调用http
Vue.prototype.$http=axios


Vue.config.productionTip = false

// 相当于全局挂载，每次都会先访问这个页面
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')


axios.interceptors.request.use(
  config => {
    if ( window.sessionStorage.getItem('Auth_Token')) {  // 判断是否存在token，如果存在的话，则每个http header都加上token
      config.headers.Auth_Token = `${window.sessionStorage.getItem('Auth_Token')}`;
    }
    return config;
  },
  err => {
    return Promise.reject(err);
  });